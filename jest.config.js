/* module.exports = {
    moduleNameMapper: {
        "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/__tests__/setup/assetsTransformer.js",
        "\\.(css|less)$": "identity-obj-proxy"
      },
      setupFiles: ["<rootDir>/__tests__/setup/setupTests.js"],
      verbose: false,
      testPathIgnorePatterns: ["/setup/", "/testData/"],
      coverageDirectory:"../../jestOutput/coverage",
     coverageReporters: ["json", "text-summary", "html","text"],
      collectCoverage: true,
      reporters: [
        "default",
        ["./node_modules/jest-html-reporter", {
          pageTitle: "Test Report",
          outputPath: "../../jestOutput/test-report.html",
          includeConsoleLog:true,
          includeFailureMsg: true,
 
        }]
      ],
     
    } */
    /**
     *
     testResultsProcessor: "./node_modules/jest-html-reporter",
 */


module.exports = {
  moduleNameMapper: {
      "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/assetsTransformer.js",
      "\\.(css|less)$": "identity-obj-proxy"
    },
    setupFiles: ["<rootDir>/__tests__/setup/setupTests.js"],
    verbose: true,
    collectCoverage: false
}