import { shape } from 'prop-types';
import { BrowserRouter } from 'react-router-dom';
import { shallow, mount, render } from 'enzyme';

// Instantiate router context
// Instantiate router context
const router = {
  history: new BrowserRouter().history,
  route: {
    location: {},
    match: {},
  },
};

const createContext = () => ({
  context: { router },
  childContextTypes: { router: shape({}) },
});

export function mountWrap(node) {
  return mount(node, createContext());
}
export function renderWrap(node) {
  return render(node, createContext());
}

export function shallowWrap(node) {
  return shallow(node, createContext());
}

export function flushAllPromises(){
   return new Promise(resolve => setImmediate(resolve));
}

export function setValue(fieldName, fieldValue) {
    return (
      { preventDefault() { }, target: { name: fieldName, value: fieldValue }})
}
export const nativeEvent = { nativeEvent: { stopImmediatePropagation: undefined } }

export function wait(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}