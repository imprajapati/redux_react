

import React from 'react';
import expect from 'expect';
import ShowTestForm from '../../src/pages/showTestForm';
import mount from 'enzyme/build/mount';



describe('showTestForm checking', () => {
  const component =mount(<ShowTestForm onChange={jest.fn()} onClick={jest.fn()}/>);
  console.error();
  it('Should chk it contains or not', function () {
    expect(component.find('Form.testing')).toHaveLength(1);
    //console.log(component.html())
  });

  it('Should capture firstname correctly onChange',function() {
    //const component = mount(<ShowTestForm />);
    const input = component.find('input').at(0);
    //input.instance().value = 'ekta';
    input.simulate('change',{target:{name:"firstName" ,value:'ekta'}});
    expect(component.state().detail.pdetail.firstName).toBe('ekta');
    console.log(component.find('input').at(0).html());
  })

  it('Should capture lastname correctly onChange', function(){
 // const component = mount(<ShowTestForm />);
  const input = component.find('input').at(1);
  //input.instance().value = 'rai';
  input.simulate('change',{target:{name:"lastName" ,value:'rai'}});
  expect(component.state().detail.pdetail.lastName).toBe('rai');
  //console.log(component.find('input').at(1).html());
  })

  it('Should capture dob correctly onChange', function(){
  //const component = mount(<ShowTestForm onChange={jest.fn()}/>);
  const input = component.find('input').at(2);
  //input.instance().value = '23/04/2018';
  input.simulate('change',{target:{name:"dob" ,value:'23/04/2018'}});
  expect(component.state().detail.pdetail.dob).toEqual('23/04/2018');
  //console.log(component.find('input').at(2).html());
  })

  it('Should capture gender correctly onChange', function(){
  /* 
  const TrustCircle = component.state
  TrustCircle.detail.pdetail.gender = "1"
  component.setState({ TrustCircle }) 
  expect(component.state().detail.pdetail.gender).toEqual('1');
  console.log(component.find('selection dropdown').html()) */
  const mockMyEventHandler = jest.fn()
  component.setProps({ onChange: mockMyEventHandler })
 // component.find('selection dropdown').prop('onChange')({data:{ value: ['val' ] }});
  component.find('dropdown').at(0).simulate('change', {data:{name:"gender", value: ['val'] }})
  expect(mockMyEventHandler).toHaveBeenCalledWith(['val'])
  //console.log(component.html())
  
  })  

  it('Should capture email correctly onChange', function(){
  const input = component.find('input').at(3);
  //input.instance().value = 'ektarai543@gmail.com';
  input.simulate('change',{target:{name:"email" ,value:'ektarai543@gmail.com'}});
  expect(component.state().detail.pdetail.email).toEqual('ektarai543@gmail.com');
 // console.log(component.find('input').at(3).html());
  })

  it('Should capture phone correctly onChange', function(){
  const input = component.find('input').at(4);
  //input.instance().value = '9453898032';
  input.simulate('change',{target:{name:"phone" ,value:'9453898032'}});
  expect(component.state().detail.pdetail.phone).toEqual('9453898032');
  //console.log(component.find('input').at(4).html());
  })

   it('check event on `Save` button', () => {
     const defaultProps = {
      handleSubmit: (fn) => fn,
      onClick: jest.fn(),
  } /* const mockMyClick = jest.fn()
  component.setProps({ onSubmit: mockMyClick }) */
    const FormButton = component.find('button').at(0);
    FormButton.simulate('click');
    expect(defaultProps.onClick).toBeCalled();
   
}); 
})