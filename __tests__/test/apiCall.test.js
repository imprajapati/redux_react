import React from 'react';
import expect from 'expect';
import { addUser, getAllUser,delUser } from '../../src/lib/services/apiCall';
import { userList,addedUser,deleteUser } from '../testData/userData';
jest.mock('../../src/lib/services/apiCall.js')

describe('API testing of projects', () => {
  beforeEach(() => {
    jest.setTimeout(9000)
  })
  it('getting userlist from API', async () => {
    const response = await getAllUser()
    console.log(response.data)
    expect(response.data[0]).toEqual(userList.data[0])
  });

  it('adding data to an API', async () => {
    const response = await addUser()
    console.log(response)
    expect(response.data[0]).toEqual(addedUser.data[0])
  }); 

  it('deleting data from an API', async () => {
    const response = await delUser()
    console.log(response)
    expect(response).toEqual(deleteUser.status)
  }); 
})
