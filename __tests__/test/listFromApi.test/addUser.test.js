import React from 'react';
import { Confirm } from 'semantic-ui-react'
import expect from 'expect';
import ShowAddUser from '../../../src/pages/showAddUser';
import { flushAllPromises, mountWrap, setValue } from '../../setup/jestHelper';

jest.mock('../../../src/lib/services/apiCall.js')

const props = { history: { push: jest.fn() }}
describe('AddUser form checking', () => {

  const wrapper = mountWrap(<ShowAddUser {...props} />);
  const onSubmitSpy = jest.spyOn(wrapper.instance(), 'handleSubmit');
  const brSpy=jest.spyOn(wrapper.instance(),'goToHome')

  it('Should chk it contains formpage or not', function () {
    expect(wrapper.find('Form')).toHaveLength(1);
  });

  it('Should capture onChange', async () => {
    wrapper.find('input#fName').simulate('change',setValue('fName', 'avantika'));
    wrapper.find('input#lName').simulate('change',setValue("lName",'singh'));
    wrapper.find('input#email').simulate('change',setValue("email",'ekta5432@gmail.com' ));
    wrapper.find('input#officePhone').simulate('change',setValue("officePhone",'9453898032'));
    wrapper.find('input#mobilePhone').simulate('change',setValue("mobilePhone",'9453898032'));
    wrapper.find('input#addrLine1').simulate('change',setValue("addrLine1",'lko'));
    wrapper.find('input#addrLine2').simulate('change',setValue("addrLine2",'up'));
    wrapper.find('input#zipCode').simulate('change',setValue("zipCode",'221002'));
    wrapper.find('input#city').simulate('change',setValue("city",'varanasi'));
    wrapper.find('input#country').simulate('change',setValue("country",'india'));
    await flushAllPromises();
    expect(wrapper.state().detail.fName).toBe('avantika');
    expect(wrapper.state().detail.lName).toBe('singh');
    expect(wrapper.state().detail.officePhone).toBe('9453898032');
    expect(wrapper.state().detail.mobilePhone).toBe('9453898032');
    expect(wrapper.state().detail.addrLine1).toBe('lko');
    expect(wrapper.state().detail.addrLine2).toBe('up');
    expect(wrapper.state().detail.zipCode).toBe('221002');
    expect(wrapper.state().detail.city).toBe('varanasi');
    expect(wrapper.state().detail.country).toBe('india');
  })

  it('Should show error msg on empty field on clicking submit button', async () => {
    wrapper.find('input#fName').simulate('change', setValue('fName', ''));
    wrapper.find('input#lName').simulate('change',setValue("lName", 'singh rai') );
    wrapper.find('button').simulate('click');
    await flushAllPromises()
    wrapper.update()
    console.log(wrapper.state())
    expect(onSubmitSpy).toHaveBeenCalled();
    expect(wrapper.find('.header').at(1).text()).toBe('Error!!');
    expect(wrapper.find('.content').at(1).text()).toBe('Error!!please fill the form correctly.');
  }) 

  it('Should check duplicate email', async () => {
    wrapper.update()
    wrapper.find('input#fName').simulate('change', setValue('fName', 'avantika'));
    wrapper.find('input#lName').simulate('change',setValue("lName", 'singh') );
    wrapper.find('input#email').simulate('change',setValue("email",'ekta543g2@gmail.com'));
    wrapper.find('button').simulate('click');
    await flushAllPromises()
    expect(onSubmitSpy).toHaveBeenCalled();
    wrapper.update()
    console.log(wrapper.state())
    expect(wrapper.find('.header').at(1).text()).toBe('Warning!!');
    expect(wrapper.find('.content').at(1).text()).toBe('Warning!!Email already exist');
    
  })

  it('Should submit the form when there is no error', async () => {
    wrapper.find('input#fName').simulate('change',setValue('fName', 'avantika'));
    wrapper.find('input#lName').simulate('change',setValue("lName", 'singh'));
    wrapper.find('input#email').simulate('change',setValue("email",'ekta5432@gmail.com'));
    wrapper.find('button').simulate('click');
    await flushAllPromises()
    expect(onSubmitSpy).toHaveBeenCalled();
    wrapper.update()
    console.log(wrapper.state())
    expect(wrapper.state().error).toEqual({ }) 
    expect(wrapper.find('.header').at(1).text()).toBe('Success!!');
    expect(wrapper.find('.content').at(1).text()).toBe('Success!!Form submitted successfully.'); 
  })

  it('Should chk breadcrumbs working', async () => {
    wrapper.update()
    wrapper.find('Breadcrumb').find('a').simulate('click');
    await flushAllPromises()
    wrapper.update()
    expect(brSpy).toHaveBeenCalled()
    expect(props.history.push.mock.calls[0]).toEqual(['/pages/newProject'])
  })

  it('modal box rendered or not', async() => {
    wrapper.update()
    await flushAllPromises()
    expect(wrapper.find('Modal')).toHaveLength(1);
  });

  it('opens modal when submit button is clicked',async () => {
    wrapper.update()
    wrapper.find('button').simulate('click');
    await flushAllPromises()
    expect(wrapper.find('Modal').prop('open')).toBe(true);
  });
})