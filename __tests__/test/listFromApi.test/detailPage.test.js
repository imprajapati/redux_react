import React from 'react';
import expect from 'expect';
import ShowDetailPage from '../../../src/pages/showDetailPage';
import { flushAllPromises, mountWrap, setValue } from '../../setup/jestHelper';
jest.mock('../../../src/lib/services/apiCall.js')

const props = {
  match: { params: { userNumber: 204 } },
  history: { push: jest.fn() }
}
describe('Update form checking', () => {

  const wrapper = mountWrap(<ShowDetailPage {...props} />);
  const updateSpy = jest.spyOn(wrapper.instance(), 'handleUpdate');
  const brSpy = jest.spyOn(wrapper.instance(), 'goToHome')


  it('Should chk wrapper rendered or not', () => {
    expect(wrapper.length).toEqual(1)
    //console.log(wrapper.html())
  })

  it('check the details renders by usernumber', async () => {
    await flushAllPromises()
    wrapper.update()
    expect(wrapper.find('input#fName').props().value).toBe('sample');
    expect(wrapper.find('input#lName').props().value).toBe('last');
    expect(wrapper.find('input#email').props().value).toBe('ekta543g2@gmail.com');
    expect(wrapper.find('input#officePhone').props().value).toBe('1234567890');
    expect(wrapper.find('input#mobilePhone').props().value).toBe('1234567890');
    expect(wrapper.find('input#addrLine1').props().value).toBe('bhju');
    expect(wrapper.find('input#addrLine2').props().value).toBe('bhju');
    expect(wrapper.find('input#zipCode').props().value).toBe('23456332');
    expect(wrapper.find('input#city').props().value).toBe('McKenziehaven');
    expect(wrapper.find('input#country').props().value).toBe('India');
    expect(wrapper.find('input#userNumber').props().value).toBe(204);
  })

  it('Should capture onChange', async () => {
    wrapper.find('input#fName').simulate('change', setValue('fName', 'avantika'));
    wrapper.find('input#lName').simulate('change', setValue("lName", 'singh'));
    wrapper.find('input#addrLine1').simulate('change', setValue("addrLine1", 'lko'));
    wrapper.find('input#addrLine2').simulate('change', setValue("addrLine2", 'up'));
    wrapper.find('input#zipCode').simulate('change', setValue("zipCode", '221002'));
    wrapper.find('input#city').simulate('change', setValue("city", 'varanasi'));
    wrapper.find('input#country').simulate('change', setValue("country", 'india'));
    await flushAllPromises();
    expect(wrapper.state().detail.fName).toBe('avantika');
    expect(wrapper.state().detail.lName).toBe('singh');
    expect(wrapper.state().detail.addrLine1).toBe('lko');
    expect(wrapper.state().detail.addrLine2).toBe('up');
    expect(wrapper.state().detail.zipCode).toBe('221002');
    expect(wrapper.state().detail.city).toBe('varanasi');
    expect(wrapper.state().detail.country).toBe('india');
  })

  it('Should show error msg on empty field on clicking submit button', async () => {
    wrapper.find('input#fName').simulate('change', setValue('fName', ''));
    wrapper.find('input#lName').simulate('change', setValue("lName", 'singh rai'));
    wrapper.find('button').simulate('click');
    await flushAllPromises()
    wrapper.update()
    console.log(wrapper.state())
    expect(updateSpy).toHaveBeenCalled();
    expect(wrapper.find('.header').at(1).text()).toBe('Error!!');
    expect(wrapper.find('.content').at(1).text()).toBe('Error!!please fill the form correctly.');
  })

  it('Should check duplicate email', async () => {
    wrapper.update()
    wrapper.find('input#fName').simulate('change', setValue('fName', 'avantika'));
    wrapper.find('input#lName').simulate('change', setValue("lName", 'singh'));
    wrapper.find('input#email').simulate('change', setValue("email", 'ekta543g2@gmail.com'));
    wrapper.find('button').simulate('click');
    await flushAllPromises()
    expect(updateSpy).toHaveBeenCalled();
    wrapper.update()
    console.log(wrapper.state())
    expect(wrapper.find('.header').at(1).text()).toBe('Error!!');
    expect(wrapper.find('.content').at(1).text()).toBe('Error!!Email already exist!!');
  })

  it('Should submit the form when there is no error', async () => {
    wrapper.update()
    wrapper.find('input#fName').simulate('change', setValue('fName', 'avantika'));
    wrapper.find('input#lName').simulate('change', setValue("lName", 'singh'));
    wrapper.find('input#email').simulate('change', setValue("email", 'avantika5432@gmail.com'));
    wrapper.find('button').simulate('click');
    await flushAllPromises()
    expect(updateSpy).toHaveBeenCalled();
    wrapper.update()
    console.log(wrapper.state())
    expect(wrapper.state().error).toEqual({})
    expect(wrapper.find('.header').at(1).text()).toBe('Success!!');
    expect(wrapper.find('.content').at(1).text()).toBe('Success!!Form updated successfully.');
  })

  it('Should chk breadcrunmb', async () => {
    wrapper.update()
    wrapper.find('Breadcrumb').find('a').simulate('click');
    await flushAllPromises()
    expect(brSpy).toHaveBeenCalled()
    expect(props.history.push.mock.calls[0]).toEqual(['/pages/getAllUsers']);
  })
})