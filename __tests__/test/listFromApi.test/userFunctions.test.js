import React from 'react';
import expect from 'expect';
import { flushAllPromises, mountWrap, setValue } from '../../setup/jestHelper';
import UserFunctions from '../../../src/comp/userFunctions';



describe('MenuPage checking', () => {

  const wrapper = mountWrap(<UserFunctions />);

  it('Should chk it contains or not', async () => {
    await flushAllPromises;
    console.log(wrapper.html())
    expect(wrapper.find('Dropdown')).toHaveLength(1);
    wrapper.find('Dropdown').simulate('click')
  });

  it('Should chk dropdown click', async () => {
    await flushAllPromises;
    const dLink0 = wrapper.find('Dropdown').find('a').at(0).simulate('click')
    expect(dLink0).toHaveLength(1)
    const dLink1 = wrapper.find('Dropdown').find('a').at(1).simulate('click')
    expect(dLink1).toHaveLength(1)
  });




})