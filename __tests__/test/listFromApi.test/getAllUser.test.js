import React from 'react';
import expect from 'expect';
import ShowGetAllUsers from '../../../src/pages/showGetAllUsers';
import { flushAllPromises, mountWrap } from '../../setup/jestHelper';

jest.mock('../../../src/lib/services/apiCall.js')
const props = { history: { push: jest.fn() }}

describe('GetAllUser page checking', () => {
  
  const wrapper = mountWrap(<ShowGetAllUsers {...props}/>);
  const deleteSpy = jest.spyOn(wrapper.instance(),'handleDelete');
  const brSpy=jest.spyOn(wrapper.instance(),'goToHome')

  it('Should chk table exist or not', function () {
    expect(wrapper.find('table')).toHaveLength(1);
  });

  it('Should chk the data rendered in the list', async()=> {
    const table = wrapper.find('table').find('tbody').find('tr').at(0)
    wrapper.update()
    await flushAllPromises
    //expect(table.find('td#userNumber').find('a').at(0)).toBe(204);
    expect(table.find('td#fName').text()).toEqual('sample');
    expect(table.find('td#lName').text()).toEqual('last');
    expect(table.find('td#email').text()).toEqual('ekta543g2@gmail.com'); 
  });  


  it('Should chk user deleted or not', async () => {
    wrapper.update()
    const table = wrapper.find('table').find('tbody').find('tr').at(0)
    console.log(table.html())
    table.find('button').simulate('click')
    await flushAllPromises()
    expect(deleteSpy).toHaveBeenCalled();
    wrapper.update()
    expect(wrapper.find('.content').text()).toBe('User deleted');
  });

  it('Should chk link rendered on next page or not', async () => {
    wrapper.update()
    const table = wrapper.find('table').find('tbody').find('tr').at(0)
    const link = table.find('td').find('a').simulate('click');
    await flushAllPromises()
    expect(link).toHaveLength(1)
  });

  it('Should chk breadcrumb"s working', async () => {
    wrapper.update()
    wrapper.find('Breadcrumb').find('a').simulate('click');
    await flushAllPromises()
    wrapper.update()
    expect(brSpy).toHaveBeenCalled()
    expect(props.history.push.mock.calls[0]).toEqual(['/pages/newProject'])
  })
 
})