import React from 'react'
import { Form, Segment, Header } from 'semantic-ui-react'


const UserCompanyDetail = (props) => {
    const { name, catchPhrase, bs } = props.companyDetail
    return (
        <div>

            <Header as='h1' color='olive'>CompanyDetail:</Header>
            <Segment>

                <Form.Input label='CompanyName' name='name' value={name} error={props.error.name} onChange={(e) => props.Change(e, 'company')} />
                <Form.Input label='CatchPhrase' name='catchPhrase' value={catchPhrase} error={props.error.catchPhrase} onChange={(e) => props.Change(e, 'company')} />
                <Form.Input label='BS' name='bs' value={bs} error={props.error.bs} onChange={(e) => props.Change(e, 'company')} />
            </Segment>
        </div>
    )


}
export default UserCompanyDetail