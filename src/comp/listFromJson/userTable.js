import React from 'react'
import { Table,Dimmer,Loader,Header, Breadcrumb} from 'semantic-ui-react'
import { Link } from 'react-router-dom';


const UserTable = (props) => {
    console.log(props.details)
    var items = props.details;
    let tabUserList
    tabUserList = items.map((itemsDetail, index) => {
        return <Table.Row>
            <Table.Cell><Link to={{ pathname :`/pages/userDetail/${itemsDetail.id}`}}>{itemsDetail.id}</Link></Table.Cell>
            <Table.Cell>{itemsDetail.name}</Table.Cell>
            <Table.Cell>{itemsDetail.phone}</Table.Cell>
            <Table.Cell>{itemsDetail.username}</Table.Cell>
            <Table.Cell>{itemsDetail.website}</Table.Cell>
        </Table.Row>

    })
    return (
        <div>
            <Breadcrumb>
            <Breadcrumb.Section active>
                    <a href='/'>Menu</a>
                </Breadcrumb.Section>
                <Breadcrumb.Divider />
                <Breadcrumb.Section link>userList</Breadcrumb.Section>
            </Breadcrumb>
            <center><Header as='h1'>USERS LIST</Header></center>
            <br></br>
            <Table celled centered>
            {props.isFetching&& <Dimmer active>
                <Loader name='loader' disabled={!props.isFetching}>Loading</Loader>
            </Dimmer>}
                <Table.Header>
                    <Table.HeaderCell>ID</Table.HeaderCell>
                    <Table.HeaderCell>NAME</Table.HeaderCell>
                    <Table.HeaderCell>CONTACT</Table.HeaderCell>
                    <Table.HeaderCell>USERNAME</Table.HeaderCell>
                    <Table.HeaderCell>WEBSITE</Table.HeaderCell>
                </Table.Header>
                <Table.Body>
                    {tabUserList}
                </Table.Body>
            </Table>
        </div>
    );


}
export default UserTable