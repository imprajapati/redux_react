import React from 'react'
import { Form, Segment, Header, Dimmer, Loader, Button, Message, Breadcrumb,Divider } from 'semantic-ui-react'

import UserAddressDetail from '../listFromJson/userAddressDetail'
import UserCompanyDetail from '../listFromJson/userCompanyDetail'

const UserDetail = (props) => {
    const { id, name, address, company, username, email, phone, website} = props.detail

    console.log(props.isFetching)
    return (
        <div><Segment>
            {props.isFetching && <Dimmer active>
                <Loader name='loader' disabled={!props.isFetching}>Loading</Loader>
            </Dimmer>}
            <Breadcrumb>
                <Breadcrumb.Section active>
                    <a href='/'>Menu</a>
                </Breadcrumb.Section>
                <Breadcrumb.Divider />
                <Breadcrumb.Section active>
                    <a href='/pages/userList'>UserList</a>
                </Breadcrumb.Section>
                <Breadcrumb.Divider />
                <Breadcrumb.Section link>UserDetail</Breadcrumb.Section>
            </Breadcrumb>
            <Form widths='equal' className={props.message.type}>
                <Message warning header={props.message.header} content={props.message.content} />
                <Message error header={props.message.header} content={props.message.content} />
                <Message success header={props.message.header} content={props.message.content} />


                <Segment>
                    <Header as='h1' color='olive'>UserDetail:</Header>

                    <Form.Group >
                        <Form.Input label='Id' name='id' value={id} error={props.error.id} onChange={props.Change} />
                        <Form.Input label='Name' name='name' value={name} error={props.error.name} onChange={props.Change} />
                    </Form.Group>
                    <Form.Group >
                        <Form.Input label='Username' name='username' value={username} error={props.error.username} onChange={props.Change} />
                        <Form.Input label='Email' name='email' value={email} error={props.error.email} onChange={props.Change} />
                    </Form.Group>
                    <Form.Group >
                        <Form.Input label='Phone' name='phone' value={phone} error={props.error.phone} onChange={props.Change} />
                        <Form.Input label='Website' name='website' value={website} error={props.error.website} onChange={props.Change} />
                    </Form.Group>

                </Segment>
                <Divider hidden />
                <UserAddressDetail
                    error={props.error}
                    addressDetail={address}
                    Change={props.Change}
                    geoChange={props.geoChange}
                />
                 <Divider hidden />
                <UserCompanyDetail
                    error={props.error}
                    companyDetail={company}
                    Change={props.Change}
                />
                 <Divider hidden />
                 <Divider hidden />
                <center><Button type='submit' content='submit' color='red' onClick={props.submit} /></center>

            </Form>
        </Segment>
        </div>
    )
}
export default UserDetail