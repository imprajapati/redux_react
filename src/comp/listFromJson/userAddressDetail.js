import React from 'react'
import { Form, Segment, Header } from 'semantic-ui-react'


const UserAddressDetail = (props) => {
          const{street,suite,city,zipcode,geo}=props.addressDetail
    return(
        <div>
            <Header as='h1' color='olive'>Address:</Header>
            <Segment>
            <Form.Group>
                    <Form.Input label='Street' name='street' value={street} error={props.error.street} onChange={(e)=>props.Change(e, 'address')}/>
                    <Form.Input label='Suite' name='suite' value={suite} error={props.error.suite} onChange={(e)=>props.Change(e, 'address')} />
                </Form.Group>
                <Form.Group>
                    <Form.Input label='City' name='city' value={city} error={props.error.city} onChange={(e)=>props.Change(e, 'address')}/>
                    <Form.Input label='Zipcode' name='zipcode' value={zipcode} error={props.error.zipcode} onChange={(e)=>props.Change(e, 'address')} />
                </Form.Group>
                <Form.Group>
                    <Form.Input label='Latitude' name='lat' value={geo.lat} error={props.error.lat} onChange={props.geoChange}/>
                    <Form.Input label='Longitude' name='lng' value={geo.lng} error={props.error.lng} onChange={props.geoChange}/>
                </Form.Group>
            </Segment>
        </div>
    )


}
export default UserAddressDetail