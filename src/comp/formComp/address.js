import React from 'react'
import { Form, Header, Segment } from 'semantic-ui-react'
import { cityOptions, stateOptions } from '../../lib/constants'

const Address = (props) => {

    return (
        <div>
            <Header as='h1' size='medium' color='black'>ADDRESS:</Header>
            <Segment>
                <Form.Group>
                    <Form.Input label='House No.' focus name="house" value={props.house} onChange={props.addressChange} error={props.error.house} />

                    <Form.Input label='Street' focus name="street" value={props.street} onChange={props.addressChange} error={props.error.street} />
                </Form.Group>

                <Form.Group>
                    <Form.Input label='Locality' focus name="locality" value={props.locality} onChange={props.addressChange} error={props.error.locality} />
                    <Form.Input label='Pincode' focus name="pincode" value={props.pincode} onChange={props.addressChange} error={props.error.pincode} />
                </Form.Group>

                <Form.Group widths='equal'>
                    <Form.Select fluid label="city" name="city" placeholder='Select city' options={cityOptions} value={props.city} onChange={props.addressSelect} error={props.error.city} />
                    <Form.Select fluid label="State" name="state" placeholder='Select state' options={stateOptions} value={props.state} onChange={props.addressSelect} error={props.error.state} />
                </Form.Group>
            </Segment>

        </div>
    )
}
export default Address