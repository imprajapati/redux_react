import React from 'react'
import { Button, Form, Header, Segment, Grid, Message } from 'semantic-ui-react'
import PersonalDetail from './personalDetail'
import Address from './address'


const ExForm = (props) => {

  return (
    <Form widths='equal' className={props.message.type}>
      <Grid centered columns={3}>
        <Grid.Column width={12}>
          <Header as='h1' textAlign='center' color='olive'> REGISTRATION FORM</Header>
          <Message warning header={props.message.header} content={props.message.content} />
          <Message error header={props.message.header} content={props.message.content} />
          <Message success header={props.message.header} content={props.message.content} />

          <PersonalDetail
            error={props.error}
            pdetail={props.pdetail}
            personalChange={props.personalChange}
            personalSelect={props.personalSelect}
            setRadio={props.setRadio}
          />
          <Address
            error={props.error}
            adetail={props.adetail}
            addressChange={props.addressChange}
            addressSelect={props.addressSelect}
          />

          <Segment>
            <Form.TextArea width={15} label='About YourSelf' placeholder='Tell us more' name="about" value={props.about} onChange={props.otherChange} error={props.error.about} />
            <Form.Checkbox label='I agree to the Terms and Conditions' name="checked" checked={props.checked} value='checked' onChange={props.setChk} error={props.error.checked} />
            <Button color="red" type='submit' content="submit" onClick={props.submit} />
          </Segment>


          <Segment >
            <Header as='h2'>Note:</Header>
            form once submitted will not be change later.
         </Segment>
        </Grid.Column>
      </Grid>
    </Form >
  )
}

export default ExForm;