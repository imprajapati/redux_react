import React from 'react'
import { Form, Header, Segment } from 'semantic-ui-react'
import { genderOptions, languageOptions } from '../../lib/constants'



const PersonalDetail = (props) => {
    return (
        <div>
            <Header as='h1' size='medium' color='black'>PERSONAL INFO:</Header>
            <Segment>

                <Form.Group>
                    <Form.Input label='First name' focus placeholder='First name' name="firstName" value={props.firstName} onChange={props.personalChange} error={props.error.firstName} />
                    <Form.Input label='Last name' focus placeholder='Last name' name="lastName" value={props.lastName} onChange={props.personalChange} error={props.error.lastName} />
                </Form.Group>

                <Form.Group widths="equal">
                    <Form.Input width={8} label='Date of Birth' placeholder='Enter your dob' name="dob" value={props.dob} onChange={props.personalChange} error={props.error.dob} />
                    <Form.Select width={8} label="Gender" placeholder='Select your gender' options={genderOptions} name="gender" value={props.gender} onChange={props.personalSelect} error={props.error.gender} />

                </Form.Group>

                <Form.Group>
                    <Form.Input label='Email' placeholder='joe@schmoe.com' name="email" value={props.email} onChange={props.personalChange} error={props.error.email} />
                    <Form.Input label='Phone' placeholder='XXXXXXXXXX' name="phone" value={props.phone} onChange={props.personalChange} error={props.error.phone} />

                </Form.Group><br></br>
                <Form.Group inline>
                    <label error={props.error.quali} ><b>Qualification&nbsp;</b></label>
                    <span style={{ color: '#9f3a38' }}><b>&nbsp;{props.error.quali}</b></span>
                </Form.Group>

                <Form.Group inline width='equal' >
                    <Form.Radio label='graduation' checked={props.pdetail.quali === 'grad'} name='quali' value='grad' onChange={props.setRadio} />
                    <Form.Radio label='post graduation' checked={props.pdetail.quali === 'post grad'} name='quali' value='post grad' onChange={props.setRadio} />
                </Form.Group>


                <Form.Group inline>
                    <label><b>Language  </b></label>
                    <Form.Dropdown placeholder='language known' scrolling multiple selection options={languageOptions} name='languageKnown' value={props.languageKnown} onChange={props.personalSelect} error={props.error.languageKnown} />
                </Form.Group>

            </Segment>
            <label></label>
        </div>
    )
}
export default PersonalDetail;