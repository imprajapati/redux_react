import React from 'react'
import { Form, Header, Segment, Button, Divider,Message ,Loader,Dimmer} from 'semantic-ui-react'
import { genderOptions } from '../lib/constants'



const TestForm = (props) => {
    console.log(props);
    
    return (
        <div>
             {props.isFetching && <Dimmer active>
                <Loader name='loader' disabled={!props.isFetching}>Loading</Loader>
            </Dimmer>}
            <Form  className={props.message.type}>
           
            <Message warning header={props.message.header} content={props.message.content} />  
              <Message error header={props.message.header} content={props.message.content} />
              <Message success header={props.message.header} content={props.message.content} />
              <Divider hidden />
                <Header as='h1' size='medium' color='black'>PERSONAL INFO:</Header>
                <Segment>
                    <Form.Group>
                        <Form.Input label='First name' focus placeholder='First name' name="firstName" value={props.pdetail.firstName} onChange={props.personalChange} error={props.error.firstName} />
                        <Form.Input label='Last name' focus placeholder='Last name' name="lastName" value={props.pdetail.lastName} onChange={props.personalChange} error={props.error.lastName} />
                    </Form.Group>
                    <Button color="red" type='change' content="change" onClick={props.change} />
                    <Form.Group >
                        <Form.Dropdown width={2} label="Gender" placeholder='Select your gender' options={genderOptions} name="gender" value={props.pdetail.gender} onChange={props.personalSelect} error={props.error.gender} />
                    </Form.Group>


                    <Form.Group>
                        <Form.Input label='Email' placeholder='joe@schmoe.com' name="email" value={props.pdetail.email} onChange={props.personalChange} error={props.error.email} />
                        <Form.Input label='Phone' placeholder='XXXXXXXXXX' name="phone" value={props.pdetail.phone} onChange={props.personalChange} error={props.error.phone} />
                    </Form.Group>
                    <Button color="red" type='submit' content="submit" onClick={props.submit} />

                </Segment>
                <label></label>
            </Form>
        </div>
    )
}
export default TestForm;