import React from 'react';
import { Link } from "react-router-dom";
import { Header, Divider, Dropdown, Segment } from 'semantic-ui-react';


const UsersFunctions = () => {

  return (
    <div>
      <Divider hidden />
      <center><Header color='teal'>MENU PAGE</Header></center>
      <Segment>
        <Dropdown text='menu' pointing>
          <Dropdown.Menu>
            <Dropdown.Header><Link to="/pages/showAddUser">AddUserPage</Link></Dropdown.Header>
            <Dropdown.Header><Link to="/pages/getAllUsers">getAllUsersPage</Link></Dropdown.Header>
          </Dropdown.Menu>
        </Dropdown>
      </Segment>
    </div>
  );
}
export default UsersFunctions