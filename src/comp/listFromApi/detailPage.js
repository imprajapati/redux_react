import React from 'react'
import { Form, Button, Link, Segment, Grid, Divider, Header, Message,Dimmer,Loader } from 'semantic-ui-react'

const DetailPage = (props) => {
	//console.log(props.detail)
	const{fName,lName,email,officePhone,mobilePhone,addrLine1,addrLine2,zipCode,city,country,userNumber}=props.error
	return (
		<div>
			{props.isFetching && <Dimmer active>
                    <Loader name='loader' disabled={!props.isFetching}>Loading</Loader>
                </Dimmer>}
			<Divider hidden />
			<Grid >
				<Grid.Column width={3}>
				</Grid.Column>
				<Grid.Column width={10}>
					<Segment color='brown'style={{backgroundColor: "lightgrey"}}>
						<Form widths='equal' className={props.message.type}>
							<center><Header as='h1' color='brown'>USERDETAIL</Header></center>
							<Divider hidden/>
							<Message warning header={props.message.header} content={props.message.content} />
							<Message error header={props.message.header} content={props.message.content} />
							<Message success header={props.message.header} content={props.message.content} />
							<Divider hidden/>
							<Header as='h1' size='medium'>PERSONAL DETAILS</Header>
							<Form.Group >
								<Form.Input label='FirstName' id='fName' name='fName' value={props.detail.fName} error={fName} onChange={props.changeDetail} />
								<Form.Input label='LastName' id='lName'  name='lName' value={props.detail.lName} error={lName} onChange={props.changeDetail} />
							</Form.Group>
							<Form.Group>
								<Form.Input label='Email' id='email'  name='email' value={props.detail.email} error={email} onChange={props.changeDetail} />
								<Form.Input label='UserNumber' id='userNumber' name='userNumber' value={props.detail.userNumber} error={userNumber} onChange={props.changeDetail} readOnly={true} />
							</Form.Group>
							<Form.Group>
								<Form.Input label='Office Contact' id='officePhone' name='officePhone' value={props.detail.officePhone} error={officePhone} onChange={props.changeDetail} />
								<Form.Input label='MobilePhone'id='mobilePhone' name='mobilePhone' value={props.detail.mobilePhone} error={mobilePhone} onChange={props.changeDetail} />
							</Form.Group>
							<Divider hidden/>
							<Header as='h1' size='medium'>ADDRESS DETAILS</Header>
							<Form.Group>
								<Form.Input label='Address Line 1' id='addrLine1' name='addrLine1' value={props.detail.addrLine1} error={addrLine1} onChange={props.changeDetail} />
								<Form.Input label='Address Line 2' id='addrLine2' name='addrLine2' value={props.detail.addrLine2} error={addrLine2} onChange={props.changeDetail} />
							</Form.Group>
							<Form.Group>
								<Form.Input label='zipCode' id='zipCode' name='zipCode' value={props.detail.zipCode} error={zipCode} onChange={props.changeDetail} />
								<Form.Input label='City' id='city' name='city' value={props.detail.city} error={city} onChange={props.changeDetail} />
							</Form.Group>
							<Form.Field width={16}>
								<label>Country</label>
								<Form.Input name='country' id='country' value={props.detail.country} error={country} onChange={props.changeDetail} />
							</Form.Field>
							<Divider hidden />
							<Button content='Update' color='olive' onClick={props.handleSubmit} />
						</Form>
						<Divider hidden/>
						<Divider hidden/>
						<Divider hidden/>
					</Segment>
				</Grid.Column>
			</Grid>
		</div>
	)
}
export default DetailPage