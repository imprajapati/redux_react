import React from 'react'
import { Table, Dimmer, Loader, Header, Divider, Button, Message, Form} from 'semantic-ui-react'
import { Link } from 'react-router-dom';


const getAllUser = (props) => {
    var items = props.details;
    let tabAllUser
    if (items.data != null || items.data != undefined) {
        tabAllUser = items.data.map((itemsDetail, index) => {
            return <Table.Row >
                      <Table.Cell id='userNumber'><Link to={{ pathname: `/pages/usersDetail/${itemsDetail.userNumber}` }}>{itemsDetail.userNumber}</Link></Table.Cell>
                      <Table.Cell id='fName'>{itemsDetail.fName}</Table.Cell>
                      <Table.Cell id='lName'>{itemsDetail.lName}</Table.Cell>
                      <Table.Cell id='email'>{itemsDetail.email}</Table.Cell>
                      <Table.Cell><Button onClick={() => props.delete(itemsDetail.userNumber)}>DELETE</Button></Table.Cell>
            </Table.Row>

        })
    }
    return (

        <div>
            {props.isFetching && <Dimmer active>
                <Loader name='loader' disabled={!props.isFetching}>Loading</Loader>
            </Dimmer>}
            <Form className={props.message.type}>
                <Divider hidden />
                <Message success content={props.message.content} />
                <center><Header as='h1'>ALL USERS</Header></center>
                <Table celled centered color='teal' rowKey="id">

                    <Table.Header>
                        <Table.HeaderCell>USERNUMBER</Table.HeaderCell>
                        <Table.HeaderCell>FIRSTNAME</Table.HeaderCell>
                        <Table.HeaderCell>LASTNAME</Table.HeaderCell>
                        <Table.HeaderCell>EMAIL</Table.HeaderCell>
                        <Table.HeaderCell >DELETE</Table.HeaderCell>
                    </Table.Header>
                    <Table.Body>
                        {tabAllUser}
                    </Table.Body>
                </Table>
            </Form>
        </div>
    );


}
export default getAllUser