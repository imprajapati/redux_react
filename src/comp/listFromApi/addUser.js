import React from 'react'
import { Form, Button, Segment, Divider, Grid, Header, Message, Breadcrumb, Dimmer, Loader, Confirm } from 'semantic-ui-react'

const AddUser = (props) => {
  const { fName, lName, email, officePhone, mobilePhone, addrLine1, addrLine2, zipCode, city, country } = props.error
  return (
    <div>
      {props.isFetching && <Dimmer active>
        <Loader name='loader' disabled={!props.isFetching}>Loading</Loader>
      </Dimmer>}
      <Breadcrumb />
      <Grid>
        <Grid.Column width={3}>
        </Grid.Column>
        <Grid.Column width={10}>
          <Segment color='brown' style={{ backgroundColor: "lightgrey" }}>
            <Form widths='equal' className={props.message.type}>
              <center><Header as='h1' color='teal'>REGISTRATION</Header></center>
              <Message warning header={props.message.header} content={props.message.content} />  
              <Message error header={props.message.header} content={props.message.content} />
              <Message success header={props.message.header} content={props.message.content} />
              <Divider hidden />
              <Header as='h1' size='medium'>PERSONAL DETAILS</Header>
              <Form.Group >
                <Form.Input label='FirstName' name='fName'id='fName' value={props.detail.fName} error={fName} onChange={props.changeDetail} />
                <Form.Input label='LastName' name='lName' id='lName' value={props.detail.lName} error={lName} onChange={props.changeDetail} />
              </Form.Group>
              <Form.Group>
                <Form.Input label='Email' name='email' id='email' value={props.detail.email} error={email} onChange={props.changeDetail} />
              </Form.Group>
              <Form.Group>
                <Form.Input label='Office Contact' name='officePhone' id='officePhone' value={props.detail.officePhone} error={officePhone} onChange={props.changeDetail} />
                <Form.Input label='MobilePhone' name='mobilePhone' id='mobilePhone' value={props.detail.mobilePhone} error={mobilePhone} onChange={props.changeDetail} />
              </Form.Group>
              <Divider hidden />
              <Header as='h1' size='medium'>ADDRESS DETAILS</Header>
              <Form.Group>
                <Form.Input label='Address Line 1' name='addrLine1' id='addrLine1' value={props.detail.addrLine1} error={addrLine1} onChange={props.changeDetail} />
                <Form.Input label='Address Line 2' name='addrLine2' id='addrLine2' value={props.detail.addrLine2} error={addrLine2} onChange={props.changeDetail} />
              </Form.Group>
              <Form.Group>
                <Form.Input label='zipCode' name='zipCode' id='zipCode' value={props.detail.zipCode} error={zipCode} onChange={props.changeDetail} />
                <Form.Input label='City' name='city' id="city" value={props.detail.city} error={city} onChange={props.changeDetail} />
              </Form.Group>
              <Form.Field width={16}>
                <label>Country5</label>
                <Form.Input name='country' id='country' value={props.detail.country} error={country} onChange={props.changeDetail} />
              </Form.Field>
              <Divider hidden />
              <Button content='Add' color='red' onClick={props.add} />
              <Confirm
                content="Do you want to continue to add more users?"
                confirmButton='Yes'
                cancelButton='No'
                open={props.isConfirm}
                onCancel={props.handleCancel}
                onConfirm={props.handleConfirm}
              />
            </Form>
          </Segment>
        </Grid.Column>
      </Grid>
    </div>
  )
}
export default AddUser