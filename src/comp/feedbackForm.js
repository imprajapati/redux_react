import React from 'react'
import{ Segment,Divider,Header, Dropdown,Button,Rating, TextArea ,Form} from 'semantic-ui-react'
import { deptOptions } from '../lib/constants'

const Feedback=(props)=>{
   
    return(
        <div><Form>
            <label>.</label>
            <Segment color='red'>
            <Header as='h1' textAlign='center' color='olive'> FEEDBACK FORM</Header>
            <Divider />
            <label><b>Select your Department:</b></label>
            <Dropdown fluid name='selectedDept' placeholder='select your department'scrolling selection options={deptOptions} onChange={props.selectChange}  value={props.detail.selectedDept} /><br/><br/>
            <label><b>Select your Faculty:</b></label>
            <Dropdown fluid name='facultyname'  placeholder='select the faculty'scrolling selection options={props.Options}  onChange={props.handleFacultyChange} value={props.detail.facultyname} /><br/><br/>
            <label><b>Performance:</b></label><br/>
            <Rating name="review" icon='star' maxRating={5}  value={props.deptDetail.review} onRate={props.reviewChange}/><br/><br/>
            <label><b>Comments:</b></label><br/>
            <TextArea  fluid name='comment'placeholder='write your comments here!!' value={props.deptDetail[props.detail.selectedDept][props.detail.facultyname].comment} onChange={props.commentChange} onClick={props.submit}/>
            <center><Button color="red" label={props.label} onClick={props.submit} primary={true}/></center>
            </Segment>
          </Form>
        </div>
    )
}
export default Feedback