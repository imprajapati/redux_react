import validator from 'validator';

export const validateUserDetail = (userDetail) => {
    let error = {}
    if ((validator.isEmpty(userDetail.pdetail.firstName)) || (!validator.isAlpha(userDetail.pdetail.firstName))) {
        error.firstName = 'firstname should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.pdetail.lastName)) || (!validator.isAlpha(userDetail.pdetail.lastName))) {
        error.lastName = 'lastname should be valid and not empty'
    }
    if (validator.isEmpty(userDetail.pdetail.dob)) {
        error.dob = 'this field should not be empty'
    }
    if (validator.isEmpty(userDetail.pdetail.gender)) {
        error.gender = 'please select gender '
    }
    if ((validator.isEmpty(userDetail.pdetail.email)) || (!validator.isEmail(userDetail.pdetail.email))) {
        error.email = 'email should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.pdetail.phone)) || (!validator.isMobilePhone(userDetail.pdetail.phone)) || (!validator.isLength(userDetail.pdetail.phone, 10, 11))) {
        error.phone = 'phone should be valid and not empty'

    }
    if (validator.isEmpty(userDetail.pdetail.quali)) {
        error.quali = "<---select qualificaion"

    }
    if ((userDetail.pdetail.languageKnown.length === 0)) {
        error.languageKnown = { content: 'this field must be filled', pointing: 'above' }
    }

    if ((validator.isEmpty(userDetail.adetail.house))) {
        error.house = 'field should not be empty'
    }


    if ((validator.isEmpty(userDetail.adetail.street)) || (!validator.isAlpha(userDetail.adetail.street))) {
        error.street = 'should be valid and not empty'
    }



    if ((validator.isEmpty(userDetail.adetail.locality)) || (!validator.isAlpha(userDetail.adetail.locality))) {
        error.locality = 'should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.adetail.pincode)) || (!validator.isNumeric(userDetail.adetail.pincode))) {
        error.pincode = 'should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.adetail.city))) {
        error.city = 'field should not be empty'
    }
    if ((validator.isEmpty(userDetail.adetail.state))) {
        error.state = 'field should not be empty'
    }
    if ((validator.isEmpty(userDetail.otherdetail.about) || (!validator.isAlpha(userDetail.otherdetail.about)))) {
        error.about = 'field should not be empty'
    }
    if ((userDetail.otherdetail.checked === false)) {
        error.checked = { content: 'this field must be filled', pointing: 'left' }

    }



    return error
}


export const validateDetail = (userDetail) => {
    let error = {}
    let objRegex = /(^[0-9]+[-]*[0-9]+$)/;
    let spaceRegex = /(^[a-z|A-Z]+(?: [a-z|A-Z]+)*$)/;
    if ((!validator.isNumeric(userDetail.id.toString()))) {
        error.id = 'id should be numeric and not empty'
    }
    if ((validator.isEmpty(userDetail.name)) || (!spaceRegex.test(userDetail.name))) {
        error.name = 'name should be valid and not empty'
    }
    if (validator.isEmpty(userDetail.username) || (!validator.isLowercase(userDetail.username)) || (!validator.isLength(userDetail.username, 5, 5))) {
        error.username = 'username should be start with lowercase and not be empty'
    }
    if ((validator.isEmpty(userDetail.email)) || (!validator.isEmail(userDetail.email))) {
        error.email = 'email should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.phone)) || (!validator.isMobilePhone(userDetail.phone)) || (!validator.isLength(userDetail.phone, 10, 11))) {
        error.phone = 'phone should be valid and not empty'

    }
    if ((validator.isEmpty(userDetail.website)) || (!validator.isURL(userDetail.website))) {
        error.website = 'please enter valid website'
    }
    if ((validator.isEmpty(userDetail.address.street)) || (!validator.isAlpha(userDetail.address.street))) {
        error.street = 'should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.address.suite))) {
        error.suite = 'should not be empty'
    }
    if ((validator.isEmpty(userDetail.address.city)) || (!validator.isAlpha(userDetail.address.city))) {
        error.city = 'should be valid and not empty'
    }

    if ((validator.isEmpty(userDetail.address.zipcode)) || (!objRegex.test(userDetail.address.zipcode))) {
        error.zipcode = 'should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.address.geo.lat)) || (validator.isLatLong(userDetail.address.geo.lat))) {
        error.lat = 'should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.address.geo.lng)) || (validator.isLatLong(userDetail.address.geo.lng))) {
        error.lng = 'should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.company.name))) {
        error.name = 'Fiels should not be empty'
    }
    if ((validator.isEmpty(userDetail.company.catchPhrase))) {
        error.catchPhrase = 'Field should not be empty'
    }
    if ((validator.isEmpty(userDetail.company.bs))) {
        error.bs = 'field should not be empty'
    }

    return error
}




export const validateAddUserDetail = (userDetail) => {
    let error = {}
    let objRegex = /(^[0-9]+[-]*[0-9]+$)/;

    if ((validator.isEmpty(userDetail.fName)) || (!validator.isAlpha(userDetail.fName))) {
        error.fName = 'name should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.lName)) || (!validator.isAlpha(userDetail.lName))) {
        error.lName = 'name should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.email)) || (!validator.isEmail(userDetail.email))) {
        error.email = 'email should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.officePhone)) || (!validator.isMobilePhone(userDetail.officePhone)) || (!validator.isLength(userDetail.officePhone, 10, 11))) {
        error.officePhone = 'phone should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.mobilePhone)) || (!validator.isMobilePhone(userDetail.mobilePhone)) || (!validator.isLength(userDetail.mobilePhone, 10, 11))) {
        error.mobilePhone = 'phone should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.addrLine1)) || (!validator.isAlpha(userDetail.addrLine1))) {
        error.addrLine1 = 'should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.addrLine2)) || (!validator.isAlpha(userDetail.addrLine2))) {
        error.addrLine2 = 'should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.city)) || (!validator.isAlpha(userDetail.city))) {
        error.city = 'should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.country)) || (!validator.isAlpha(userDetail.country))) {
        error.country = 'should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.zipCode)) || (!objRegex.test(userDetail.zipCode))) {
        error.zipCode = 'should be valid and not empty'
    }


    return error
}

export const validateShowDetail = (userDetail) => {
    let error = {}
    let objRegex = /(^[0-9]+[-]*[0-9]+$)/;

    if ((validator.isEmpty(userDetail.fName)) || (!validator.isAlpha(userDetail.fName))) {
        error.fName = 'name should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.lName)) || (!validator.isAlpha(userDetail.lName))) {
        error.lName = 'name should be valid and not empty'
    }
    /* if ((validator.isEmpty(userDetail.userNumber)) || (!validator.isNumeric(userDetail.userNumber))) {
        error.userNumber = 'usernumber should be valid and not empty'
    } */
    if ((validator.isEmpty(userDetail.email)) || (!validator.isEmail(userDetail.email))) {
        error.email = 'email should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.officePhone)) || (!validator.isMobilePhone(userDetail.officePhone)) || (!validator.isLength(userDetail.officePhone, 10, 11))) {
        error.officePhone = 'phone should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.mobilePhone)) || (!validator.isMobilePhone(userDetail.mobilePhone)) || (!validator.isLength(userDetail.mobilePhone, 10, 11))) {
        error.mobilePhone = 'phone should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.addrLine1)) || (!validator.isAlpha(userDetail.addrLine1))) {
        error.addrLine1 = 'should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.addrLine2)) || (!validator.isAlpha(userDetail.addrLine2))) {
        error.addrLine2 = 'should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.city)) || (!validator.isAlpha(userDetail.city))) {
        error.city = 'should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.country)) || (!validator.isAlpha(userDetail.country))) {
        error.country = 'should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.zipCode)) || (!objRegex.test(userDetail.zipCode))) {
        error.zipCode = 'should be valid and not empty'
    }


    return error
}



export const validatePuserDetail = (userDetail) => {
    let error = {}
    if ((validator.isEmpty(userDetail.pdetail.firstName)) || (!validator.isAlpha(userDetail.pdetail.firstName))) {
        error.firstName = 'firstname should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.pdetail.lastName)) || (!validator.isAlpha(userDetail.pdetail.lastName))) {
        error.lastName = 'lastname should be valid and not empty'
    }
    if (validator.isEmpty(userDetail.pdetail.gender)) {
        error.gender = 'please select gender '
    }
    if ((validator.isEmpty(userDetail.pdetail.email)) || (!validator.isEmail(userDetail.pdetail.email))) {
        error.email = 'email should be valid and not empty'
    }
    if ((validator.isEmpty(userDetail.pdetail.phone)) || (!validator.isMobilePhone(userDetail.pdetail.phone)) || (!validator.isLength(userDetail.pdetail.phone, 10, 11))) {
        error.phone = 'phone should be valid and not empty'

    }
    return error
}