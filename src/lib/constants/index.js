export const cityOptions = [
    { key: 'Lucknow', text: 'Lucknow', value: 'Lucknow' },
    { key: 'Kanpur', text: 'Kanpur', value: 'Kanpur' },
    { key: 'Varanasi', text: 'Varanasi', value: 'Varanasi' },
]
export  const stateOptions = [
    { key: 'UttarPradesh', text: 'UttarPradesh', value: 'UttarPradesh' },
    { key: 'andhraPradesh', text: 'andhraPradesh', value: 'andhraPradesh' },
    { key: 'ArunanchalPraedsh', text: 'ArunanchalPraedsh', value: 'ArunanchalPraedsh' }
]
export const genderOptions = [
    { key: 'male', text: 'Male', value: 'male' },
    { key: 'female', text: 'Female', value: 'female' },
    { key: 'other', text: 'Other', value: 'other' },
]

export const languageOptions = [
    { key: 'angular', text: 'Angular', value: 'angular' },
    { key: 'css', text: 'CSS', value: 'css' },
    { key: 'design', text: 'Graphic Design', value: 'design' },
    { key: 'ember', text: 'Ember', value: 'ember' },
    { key: 'html', text: 'HTML', value: 'html' },
    { key: 'ia', text: 'Information Architecture', value: 'ia' },
    { key: 'javascript', text: 'Javascript', value: 'javascript' },
    { key: 'mech', text: 'Mechanical Engineering', value: 'mech' },
    { key: 'meteor', text: 'Meteor', value: 'meteor' },
    { key: 'node', text: 'NodeJS', value: 'node' },
    { key: 'plumbing', text: 'Plumbing', value: 'plumbing' },
    { key: 'python', text: 'Python', value: 'python' },
    { key: 'rails', text: 'Rails', value: 'rails' },
    { key: 'react', text: 'React', value: 'react' },
    { key: 'repair', text: 'Kitchen Repair', value: 'repair' },
    { key: 'ruby', text: 'Ruby', value: 'ruby' },
    { key: 'ui', text: 'UI Design', value: 'ui' },
    { key: 'ux', text: 'User Experience', value: 'ux' },
]
export const nameOptions = [
    { key: 'abc', text: 'abc', value: 'abc' },
    { key: 'def', text: 'def', value: 'def' },
    { key: 'ghi', text: 'ghi', value: 'ghi' },
]

export const deptOptions = [
    { key: 'CS', text: 'CS', value: 'CS' },
    { key: 'IT', text: 'IT', value: 'IT' },
    { key: 'EC', text: 'EC', value: 'EC' },
]

/* export const facultyOptions = [
    { key: 'Daniel', text: 'Daniel', value: 'Daniel' },
    { key: 'mark', text: 'mark', value: 'mark' },
    { key: 'harry', text: 'harry', value: 'harry' },
    { key: 'tom', text: 'tom', value: 'tom' },
] */

export const CSFaculty = [
{
    key:'Ekta',value:'Ekta',text:'Ekta'
},
{
    key:'Yash',value:'Yash',text:'Yash'
}
]
export const ITFaculty = [
    { key: 'satyarth', text: 'satyarth', value: 'satyarth' },
    { key: 'suraj', text: 'suraj', value: 'suraj' },
  
]