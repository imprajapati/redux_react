import axios from 'axios'


export const getUsers = () => {

return axios.get('https://jsonplaceholder.typicode.com/users')
            .then(response => {
                  return response.data
            })
            .catch((err) => {
            return err
            })

}

export const getUserDetail =(id) => {

      return axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
                  .then(response => {
                        return response.data
                  })
                  .catch((err) => {
                  return err
                  })
      
      }


export const addUser = (detail) => {

      return axios.post(' https://dev1blocksecure.symblock.com/api/v1/training/addUser',detail,{headers:{Authorization:`Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmdOdW1iZXIiOjYwLCJzY29wZSI6WyJ1cGRhdGU6cHJvdmlkZXIiLCJyZWFkOnByb3ZpZGVyIiwidXBkYXRlOmFsbCJdLCJzZXNzaW9uS2V5Q2xpZW50IjoiYzUzNWY2MjMwNGUwNjYwYzA2MGNmMjA5OGQ1NzFmMDkiLCJ0eXBlIjoiZXh0QVBJU2VydmljZUFjY3QiLCJleHAiOjE2Njk4NzEyMDAsImlhdCI6MTU4Mjk1NzIwMn0.ycYZU7Ru3F7P-rv4scUUS0dKNMGaLPApSihPh5i9MeE`}})
                  .then(response => {
                        console.log(response.data) 
                   })
                  .catch((err) => {
                  return err
                  })
            
      }

      export const getAllUser =() => {

            return axios.post(`https://dev1blocksecure.symblock.com/api/v1/training/getAllUsers`)
                        .then(response => {
                              
                              return response.data
                              
                         })
                        .catch((err) => {
                        return err
                        })
                  
            }      


        export const delUser = (userNumber) => {
            return axios.post(`https://dev1blocksecure.symblock.com/api/v1/training/deleteUser`, {userNumber},{headers:{Authorization:`Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmdOdW1iZXIiOjYwLCJzY29wZSI6WyJ1cGRhdGU6cHJvdmlkZXIiLCJyZWFkOnByb3ZpZGVyIiwidXBkYXRlOmFsbCJdLCJzZXNzaW9uS2V5Q2xpZW50IjoiYzUzNWY2MjMwNGUwNjYwYzA2MGNmMjA5OGQ1NzFmMDkiLCJ0eXBlIjoiZXh0QVBJU2VydmljZUFjY3QiLCJleHAiOjE2Njk4NzEyMDAsImlhdCI6MTU4Mjk1NzIwMn0.ycYZU7Ru3F7P-rv4scUUS0dKNMGaLPApSihPh5i9MeE`}})
                        .then(response => {
                              return response.data
                                    
                         })
                        .catch((err) => {
                        return err
                        })
                        
            }     
            
            
            export const userDeta = (userNumber) => {
                  return axios.get( `https://dev1blocksecure.symblock.com/api/v1/training/user/${userNumber}`,{},{headers:{Authorization:`Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmdOdW1iZXIiOjYwLCJzY29wZSI6WyJ1cGRhdGU6cHJvdmlkZXIiLCJyZWFkOnByb3ZpZGVyIiwidXBkYXRlOmFsbCJdLCJzZXNzaW9uS2V5Q2xpZW50IjoiYzUzNWY2MjMwNGUwNjYwYzA2MGNmMjA5OGQ1NzFmMDkiLCJ0eXBlIjoiZXh0QVBJU2VydmljZUFjY3QiLCJleHAiOjE2Njk4NzEyMDAsImlhdCI6MTU4Mjk1NzIwMn0.ycYZU7Ru3F7P-rv4scUUS0dKNMGaLPApSihPh5i9MeE`}})
                              .then(response => {
                                    return response.data
                                    
                               })
                              .catch((err) => {
                              return err
                              })
                        
                  }      

                  export const updateUser = (detail) => {
                        return axios.post( ` https://dev1blocksecure.symblock.com/api/v1/training/updateUser`,detail,{headers:{Authorization:`Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmdOdW1iZXIiOjYwLCJzY29wZSI6WyJ1cGRhdGU6cHJvdmlkZXIiLCJyZWFkOnByb3ZpZGVyIiwidXBkYXRlOmFsbCJdLCJzZXNzaW9uS2V5Q2xpZW50IjoiYzUzNWY2MjMwNGUwNjYwYzA2MGNmMjA5OGQ1NzFmMDkiLCJ0eXBlIjoiZXh0QVBJU2VydmljZUFjY3QiLCJleHAiOjE2Njk4NzEyMDAsImlhdCI6MTU4Mjk1NzIwMn0.ycYZU7Ru3F7P-rv4scUUS0dKNMGaLPApSihPh5i9MeE`}})
                                    .then(response => {
                                          return response.data
                                          
                                     })
                                    .catch((err) => {
                                    return err
                                    })
                              
                        }            