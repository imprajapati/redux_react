import React, { Component } from 'react'
import ExForm from '../comp/formComp/Form'
import update from 'immutability-helper';

import { validateUserDetail } from '../lib/util'


class Registration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: {},
            msg: {
                type: '',
                header: '',
                content: ''
            },
            detail: {
                pdetail: {
                    firstName: '',
                    lastName: '',
                    dob: '',
                    gender: '',
                    email: '',
                    phone: '',
                    quali: '',
                    languageKnown: [],
                },
                adetail: {
                    house: '',
                    street: '',
                    locality: '',
                    pincode: '',
                    city: '',
                    state: '',
                },
                otherdetail: {
                    about: '',
                    checked: false,
                }
            }
        }


    }

    removeError = (errorField) => {
        if (this.state.error[errorField] !== undefined) {
            delete (this.state.error[errorField])
        }
        if (Object.keys(this.state.error).length === 0) {
            this.setState({ msg: update(this.state.msg, { type: { $set: 'hidden' } }) })
        }
    }

    handleRadio = (e, data) => {
        this.setState({
            detail: update(this.state.detail, {
                pdetail: { [data.name]: { $set: data.value } }
            })
        })
        this.removeError(data.name);
    }

    handleChk = (e, data) => {
        this.setState({
            detail: update(this.state.detail, {
                otherdetail: { [data.name]: { $set: data.checked } }
            })
        })
        this.removeError(data.name);
    }


    handlePchange = (e) => {
        this.setState({
            detail: update(this.state.detail, {
                pdetail: { [e.target.name]: { $set: e.target.value } }
            })
        });
        this.removeError(e.target.name);
    }

    handleAchange = (e) => {
        this.setState({
            detail: update(this.state.detail, {
                adetail: { [e.target.name]: { $set: e.target.value } }
            })
        });
        this.removeError(e.target.name);
    }

    handleOchange = (e) => {
        this.setState({
            detail: update(this.state.detail, {
                otherdetail: { [e.target.name]: { $set: e.target.value } }
            })
        });
        this.removeError(e.target.name);
    }


    handlePselectChange = (e, data) => {
        this.setState({
            detail: update(this.state.detail, {
                pdetail: { [data.name]: { $set: data.value } }

            })
        })
        this.removeError(data.name);
    }


    handleAselectChange = (e, data) => {
        this.setState({
            detail: update(this.state.detail, {
                adetail: { [data.name]: { $set: data.value } }
            })
        })
        this.removeError(data.name);
    }

    handleSubmit = () => {
        let error = validateUserDetail(this.state.detail)
        console.log(this.state.detail)
        if (Object.keys(error).length !== 0) {
            this.setState({ error: error });
            this.setState({ msg: update(this.state.msg, { type: { $set: 'error' }, header: { $set: 'Error!!' }, content: { $set: 'please fill the form correctly.' } }) })
        }
        else {
            this.setState({ msg: update(this.state.msg, { type: { $set: 'success' }, header: { $set: 'Success!!' }, content: { $set: 'Form submitted successfully.' } }) })
        }
    }



    render() {

        return (
            <div>
                <ExForm
                    error={this.state.error}
                    message={this.state.msg}
                    submit={this.handleSubmit}
                    personalSelect={this.handlePselectChange}
                    addressSelect={this.handleAselectChange}
                    personalChange={this.handlePchange}
                    addressChange={this.handleAchange}
                    otherChange={this.handleOchange}
                    setRadio={this.handleRadio}
                    Msg={this.handleMsg}
                    setChk={this.handleChk}
                    detail={this.state.detail}
                    pdetail={this.state.detail.pdetail}
                    adetail={this.state.detail.adetail}
                    otherdetail={this.state.detail.otherdetail}
                />
            </div>
        );

    }

}
export default Registration