import React, { Component } from 'react'
import TestForm from '../comp/testForm'
import update from 'immutability-helper';
import { validatePuserDetail } from '../lib/util'
import { connect } from 'react-redux'
import { changeName } from '../store/actions/actionLoader'

class showTestForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loader: false,
			error: {},
			msg: {
				type: '',
				header: '',
				content: ''
			},
			detail: {
				pdetail: {
					firstName: 'Shailendra',
					lastName: '',
					gender: '',
					email: '',
					phone: ''
				}
			}
		}
	}

	removeError = (errorField) => {
		if (this.state.error[errorField] !== undefined) {
			delete (this.state.error[errorField])
		}
		if (Object.keys(this.state.error).length === 0) {
			this.setState({ msg: update(this.state.msg, { type: { $set: 'hidden' } }) })
		}
	}
	handleChange = async (e) => {
		await this.props.changeName()
		this.setState({
			detail: update(this.state.detail, {
				pdetail: { 'firstName': { $set: this.props.state.firstName } }
			})
		})
	}

	handlePchange = (e) => {
		this.setState({
			detail: update(this.state.detail, {
				pdetail: { [e.target.name]: { $set: e.target.value } }
			})
		});
		this.removeError(e.target.name);
	}



	handlePselectChange = (e, data) => {
		this.setState({
			detail: update(this.state.detail, {
				pdetail: { [data.name]: { $set: data.value } }

			})
		})
		this.removeError(data.name);
	}



	handleSubmit = () => {
		console.log('button called');
		this.setState({ loader: true })
		let error = validatePuserDetail(this.state.detail)
		if ((Object.keys(error).length !== 0)) {
			this.setState({ error: error });
			this.setState({ loader: false, msg: update(this.state.msg, { type: { $set: 'error' }, header: { $set: 'Error!!' }, content: { $set: 'please fill the form correctly.' } }) })
		}
		else {
			//this.setState({ detail: update(this.state.detail, {pdetail: { [e.target.name]: { $set: e.target.value } }})
			this.setState({ loader: false, msg: update(this.state.msg, { type: { $set: 'success' }, header: { $set: 'Success!!' }, content: { $set: 'Form submitted successfully.' } }) })
		}
	}



	render() {
		console.log(this.props.state);

		return (
			<div>
				<TestForm
					error={this.state.error}
					message={this.state.msg}
					submit={this.handleSubmit}
					change={this.handleChange}
					personalSelect={this.handlePselectChange}
					personalChange={this.handlePchange}
					pdetail={this.state.detail.pdetail}
					isFetching={this.state.loader}
				/>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		state
	}
}

const mapDispatchToProps = {changeName}


// export default showTestForm;
export default connect(mapStateToProps, mapDispatchToProps)(showTestForm)