import React, { Component } from 'react'
import AddUser from '../comp/listFromApi/addUser'
import update from 'immutability-helper'
import { validateAddUserDetail } from '../lib/util/index'
import { addUser, getAllUser } from '../lib/services/apiCall';
import { Breadcrumb, Button } from 'semantic-ui-react';

const userDetail = {
    fName: '',
    lName: '',
    email: 'ekta5432@gmail.com',
    officePhone: '',
    mobilePhone: '',
    addrLine1: '',
    addrLine2: '',
    zipCode: '',
    city: '',
    country: '',
}

class ShowAddUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: {},
            msg: {
                type: '',
                header: '',
                content: ''
            },
            open: false,
            loader: false,
            detail: userDetail
        }
    }
    handleConfirm = () => {
        this.setState({ open: false, detail: userDetail })
        this.setState({ msg: update(this.state.msg, { type: { $set: 'hidden' }}) })
    }
    handleCancel = () => {
        this.setState({ open: false })
        this.props.history.push('/pages/getAllUsers');
    }


    removeError = (errorField) => {
        if (this.state.error[errorField] !== undefined) {
            delete (this.state.error[errorField])
        }
        if (Object.keys(this.state.error).length === 0) {
            this.setState({ msg: update(this.state.msg, { type: { $set: 'hidden' } }) })
        }
    }

    handleChange = (e) => {
        console.log('on change function called')
        this.setState({ detail: update(this.state.detail, { [e.target.name]: { $set: e.target.value } }) })
        this.removeError(e.target.name);
    }

    goToHome() {
        console.log('qwert');
        this.props.history.push('/pages/newProject');
    }

    duplicateEmail() {

        return getAllUser()

            .then(response => {
               // console.log(response);
                let isDupli = response.data.findIndex(i => i.email === this.state.detail.email)
                return isDupli
            })
            .catch((err) => {
                this.setState({ msg: update(this.state.msg, { type: { $set: 'error' }, header: { $set: 'Error!!' }, content: { $set: 'Something went wrong' } }) })
                return err
            })
    }

    handleSubmit = () => {
        console.log('on submit function called')
        this.setState({ loader: true })
        let error = validateAddUserDetail(this.state.detail)
        //console.log(error)
        if (Object.keys(error).length !== 0) {
            this.setState({ error: error });
            this.setState({ loader: false, msg: update(this.state.msg, { type: { $set: 'error' }, header: { $set: 'Error!!' }, content: { $set: 'please fill the form correctly.' } }) })
        }
        else {
            return this.duplicateEmail()
                .then(response => {
                    //console.log(response,typeof response);
                    if (response !== -1)
                        this.setState({ loader: false, msg: update(this.state.msg, { type: { $set: 'warning' }, header: { $set: 'Warning!!' }, content: { $set: 'Email already exist' } }) })
                    else {
                        return addUser(this.state.detail)
                            .then(response => {
                                console.log(response)
                                this.setState({ open: true, loader: false, msg: update(this.state.msg, { type: { $set: 'success' }, header: { $set: 'Success!!' }, content: { $set: 'Form submitted successfully.' } }) })
                            })
                            .catch((err) => {
                                this.setState({ msg:update(this.state.msg, { type: { $set: 'error' }, header: { $set: 'Error!!' }, content: { $set: 'Something went wrong' } }) })
                                return err
                            })

                    }
                })
                .catch((err) => {
                    this.setState({ msg: update(this.state.msg, { type: { $set: 'error' }, header: { $set: 'Error!!' }, content: { $set: 'Something went wrong' } }) })
                    return err
                })




        }

    }

    render() {
        
        return (
            <div>

                <Breadcrumb>
                    <Breadcrumb.Section onClick={() => { this.goToHome() }}>Menu</Breadcrumb.Section>
                    <Breadcrumb.Divider></Breadcrumb.Divider>
                    <Breadcrumb.Section>Add User</Breadcrumb.Section>
                </Breadcrumb>


                <AddUser
                    message={this.state.msg}
                    error={this.state.error}
                    detail={this.state.detail}
                    changeDetail={this.handleChange}
                    add={this.handleSubmit}
                    isFetching={this.state.loader}
                    isConfirm={this.state.open}
                    handleConfirm={this.handleConfirm}
                    handleCancel={this.handleCancel}
                />

            </div>
        )
    }
}
export default ShowAddUser