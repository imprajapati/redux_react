import React, { Component } from 'react'
import DetailPage from '../comp/listFromApi/detailPage'
import update from 'immutability-helper'
import { validateShowDetail } from '../lib/util/index'
import { userDeta, updateUser, getAllUser } from '../lib/services/apiCall'
import { Breadcrumb } from 'semantic-ui-react'

class ShowDetailPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: {},
            msg: {
                type: '',
                header: '',
                content: ''
            },
            loader: false,
            detail: {
                fName: '',
                lName: '',
                email: '',
                officePhone: '',
                mobilePhone: '',
                addrLine1: '',
                addrLine2: '',
                zipCode: '',
                city: '',
                country: '',
                userNumber: ''
            }
        }
    }

    removeError = (errorField) => {
        if (this.state.error[errorField] !== undefined) {
            delete (this.state.error[errorField])
        }
        if (Object.keys(this.state.error).length === 0) {
            this.setState({ msg: update(this.state.msg, { type: { $set: 'hidden' } }) })
        }
    }

    handleChange = (e) => {
        this.setState({ detail: update(this.state.detail, { [e.target.name]: { $set: e.target.value } }) })
        this.removeError(e.target.name)
    }

    isDuplicate() {
        return getAllUser()
            .then(response => {
                let isDupli = response.data.findIndex(i => i.email === this.state.detail.email)
                return isDupli
            })
    }

    componentDidMount = () => {
        this.getDetails()
    }
    getDetails = () => {
        this.setState({ loader: true })
        return userDeta(this.props.match.params.userNumber)
            .then(response => {
                console.log(response)
                this.setState({ detail: response.data, loader: false })
            })
            .catch((err) => {
                console.log('error')
                this.setState({ msg: update(this.state.msg, { type: { $set: 'error' }, header: { $set: 'Error!!' }, content: { $set: 'Something went wrong' } }) })
                return err
            })

    }
    goToHome() {
        console.log('qwert');
        this.props.history.push('/pages/getAllUsers');
    }

    handleUpdate = () => {
        console.log('update button called')
        let error = validateShowDetail(this.state.detail)
        console.log(this.state.detail)
        if (Object.keys(error).length !== 0) {
            this.setState({ error: error });
            this.setState({ msg: update(this.state.msg, { type: { $set: 'error' }, header: { $set: 'Error!!' }, content: { $set: 'please fill the form correctly.' } }) })
        }
        else {
            return this.isDuplicate()
                .then(response => {
                    if (response !== -1)
                        this.setState({ msg: update(this.state.msg, { type: { $set: 'error' }, header: { $set: 'Error!!' }, content: { $set: 'Email already exist!!' } }) })
                    else {
                        this.setState({ loader: true })

                        return updateUser(this.state.detail)
                            .then(response => {
                                this.setState({ loader: false })

                                this.setState({ msg: update(this.state.msg, { type: { $set: 'success' }, header: { $set: 'Success!!' }, content: { $set: 'Form updated successfully.' } }) })
                            })
                            .catch((err) => {
                                this.setState({ msg: update(this.state.msg, { type: { $set: 'error' }, header: { $set: 'Error!!' }, content: { $set: 'Something went wrong' } }) })
                                return err
                            })
                    }
                })
                .catch(err => {
                    return err
                })



        }
    }


    render() {

        return (
            <div>
                <Breadcrumb>
                    <Breadcrumb.Section onClick={() => { this.goToHome() }}>AllUsers</Breadcrumb.Section>
                    <Breadcrumb.Divider></Breadcrumb.Divider>
                    <Breadcrumb.Section>UserDetail</Breadcrumb.Section>
                </Breadcrumb>
                <DetailPage
                    error={this.state.error}
                    message={this.state.msg}
                    detail={this.state.detail}
                    changeDetail={this.handleChange}
                    handleSubmit={this.handleUpdate}
                    isFetching={this.state.loader}
                />
            </div>
        )
    }
}

export default ShowDetailPage