import React, { Component } from 'react'
import { getAllUser,delUser} from '../lib/services/apiCall';
import GetAllUser from '../comp/listFromApi/getAllUser';
import {Breadcrumb} from 'semantic-ui-react'
import update from 'immutability-helper'

class ShowGetAllUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader:false,
            msg:{
                type:'',
                header:'',
                content:''
            },
            detail:[{
                fName:'',
                lName:'',
                email:'',
                officePhone:'',
                mobilePhone:'',
                addrLine1:'',
                addrLine2:'',
                zipCode:'',
                city:'',
                country:'',
                userNumber:''
            }]
        }
    }
   
    componentDidMount = () => {
       this.getUser()
    }
    getUser=()=>{
        this.setState({ loader: true})
        return getAllUser()
        .then(response => {
          this.setState({detail:response,loader:false})
         
        })
        .catch((err) => {
            console.log('error')
            return err
        })  
    }

    handleDelete=(userNumber)=>{      
        console.log("button called")  
        return delUser(userNumber)
        .then(response => {
          this.getUser()
          this.setState({ msg: {type:'success',content:"User deleted"}})
        })
        .catch((err) => {
            this.setState({msg: update(this.state.msg, { type: { $set: 'error' }, header: { $set: 'Error!!' }, content: { $set: 'Something went wrong' } }) })
            return err
        })
    }
    goToHome(){
        console.log('qwert');
        this.props.history.push('/pages/newProject');
    }
    render() {
        return (
            <div>
                  <Breadcrumb>
                    <Breadcrumb.Section onClick={()=>{this.goToHome()}}>Menu</Breadcrumb.Section>
                    <Breadcrumb.Divider></Breadcrumb.Divider>
                    <Breadcrumb.Section>All Users</Breadcrumb.Section>
                </Breadcrumb>
                <GetAllUser
                isFetching={this.state.loader}
                details={this.state.detail}
                delete={this.handleDelete}
                message={this.state.msg}
                />
            </div>
        );
    }


}

export default ShowGetAllUsers