import React, { Component } from 'react'
import Feedback from '../comp/feedbackForm'
import update from 'immutability-helper';
import {CSFaculty, ITFaculty} from '../lib/constants'

class ShowFeedback extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDept:'CS',
            facultyname:'Ekta',
            label:'submit',
            disabled:true,
            deptDtls: {
                CS: {
                    Ekta: {
                        comment: 'best',
                        review:5
                    },
                    Yash: {
                        comment: 'best',
                        review: 5
                    }
                 },
                IT:{
                    satyarth:{
                        comment: 'best',
                        review: 5 
                    },
                    suraj:{
                        comment: 'best',
                        review: 5 
                    }
                }
            },
            options:CSFaculty
        }
        

    }

handleSelectChange=(e,data)=>{

let facultyOptions
if(data.value==='IT')
facultyOptions = ITFaculty

else if(data.value==='CS')
facultyOptions = CSFaculty
this.setState({[data.name]:data.value,options:facultyOptions,facultyname:facultyOptions[0].value})

}

handleFacultyChange=(e,data)=>{
    this.setState({[data.name]:data.value})
}

handleCchange=(e,data)=>{
    this.setState({deptDtls:update(this.state.deptDtls,{[this.state.selectedDept]:{[this.state.facultyname]:{comment:{$set:data.value}}}})})
    
}
handleRchange=(e,data)=>{
    this.setState({deptDtls:update(this.state.deptDtls,{[this.state.selectedDept]:{[this.state.facultyname]:{review:{$set:data.rating}}}})})
    
}
handleSubmit=()=>{
    this.setState({disabled: !this.state.disabled, label: 'update'});
}

    render() {
        console.log(this.state) 
        return (<Feedback
            commentChange={this.handleCchange}
            reviewChange={this.handleRchange}
            label={this.state.label}
            Options={this.state.options}
            submit={this.handleSubmit}
            detail={this.state}
            handleFacultyChange={this.handleFacultyChange}
            deptDetail={this.state.deptDtls}
            selectChange={this.handleSelectChange}
            />)
    }

}
export default ShowFeedback