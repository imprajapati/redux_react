import React, { Component } from 'react'
import Bmi from '../comp/bmiForm'
import update from 'immutability-helper';

class ShowBmi extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
                fname: '',
                desc:[{
                name:'',
                height: '',
                wieght: '',
                bmiValue:'',
                    }]
            
        }
    }
    
    handleDropdown = (e,data) => {
        
       this.setState({[data.name]:data.value})
        var ifnameExist=this.state.desc.some(c=>c.name===data.value)
        if(!ifnameExist)
        {
            this.setState({desc:update(this.state.desc,{
                $push:[{name:data.value,height:'',wieght:'',bmiValue:''}]
            })})
        }
    }

    handleChange = (e) => {
        var index=this.state.desc.findIndex(i=>i.name===this.state.fname)
        this.setState({
            desc: update(this.state.desc,{[index]:{[e.target.name]:{$set:e.target.value}}} 
            )
        })

    }

    computeBmi=()=> {
        var index=this.state.desc.find(d=>d.name===this.state.fname)
        let bmi = (index.wieght/(index.height * index.height))
         console.log(bmi)
         var index1=this.state.desc.findIndex(i=>i.name===this.state.fname)
        this.setState({ desc: update(this.state.desc,{[index1]:{bmiValue: { $set: bmi }} }) })
        console.log(this.state)
    }

    handleSubmit = () => {
        this.computeBmi()

    }

    render() {

        return (
            <div>
                <Bmi
                    details={this.state}
                    handleChange={this.handleChange}
                    handleDropdown={this.handleDropdown}
                    handleSubmit={this.handleSubmit}
                    computeBmi={this.computeBmi}
                />

            </div>
        )
    }

}
export default ShowBmi