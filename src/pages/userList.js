import React, { Component } from 'react'
import { getUsers } from '../lib/services/apiCall';
import UserTable from '../comp/listFromJson/userTable';


class UserList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader:false,
            allUsers: [{
                id:'',
                name:'',
                phone:'',
                username:'',
                website:''
            }]
        }
    }
   
    componentDidMount = () => {
        this.setState({ loader: true})
        return getUsers()
            .then(response => {
               /*  console.log(response);  */
              this.setState({allUsers:response,loader:false})
             
            })
            .catch((err) => {
                console.log('error')
                return err
            })
           
    }
    render() {
        console.log(this.state.allUsers)
        return (
            <div>
                <UserTable
                isFetching={this.state.loader}
                 details={this.state.allUsers}
                />
            </div>
        );
    }


}

export default UserList