import React, { Component } from 'react'
import UserDetail from '../comp/listFromJson/userDetail'
import {getUserDetail} from '../lib/services/apiCall';
import {validateDetail} from '../lib/util/index'
import update from 'immutability-helper'


class showUserDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader:false,
            error:{},
            msg: {
                type: '',
                header: '',
                content: ''
            },
            userDetail: {
                id: '',
                name:'',
                username:'',
                email:'',
                address: {
                    street:'',
                    suite:'',
                    city:'',
                    zipcode:'',
                    geo:{
                        lat:'',
                        lng:''
                      }
                },
                phone:'',
                website:'',
                company: {
                    name:'',
                    catchPhrase: '',
                    bs:''
                }
            }
        }
    }

    removeError = (errorField) => {
        if (this.state.error[errorField] !== undefined) {
            delete (this.state.error[errorField])
        }
        if (Object.keys(this.state.error).length === 0) {
            this.setState({ msg: update(this.state.msg, { type: { $set: 'hidden'} }) })
        }
    }

    componentDidMount = () => {
        this.setState({ loader: true})
        return getUserDetail(this.props.match.params.id)
            .then(response => {
                this.setState({ userDetail: response, loader: false})
            })
            .catch((err) => {
                console.log('error')
                return err
            })

    }
    
    
    handleACchange=(e, fieldName)=>{
        if(fieldName==="address"||fieldName==="company")
        {
        this.setState({userDetail:update(this.state.userDetail,{[fieldName]:{[e.target.name]:{$set:e.target.value}}})})
        this.removeError(e.target.name)
        }else
        {
            this.setState({userDetail:update(this.state.userDetail,{[e.target.name]:{$set:e.target.value}})})
           this.removeError(e.target.name)
        }
    }
    handlegeoChange=(e)=>{
        this.setState({userDetail:update(this.state.userDetail,{address:{geo:{[e.target.name]:{$set:e.target.value}}}})})
        this.removeError(e.target.name)
    }
    handleSubmit=()=>{
        let error = validateDetail(this.state.userDetail)
        this.setState({ error: error });
        console.log(error)
        if (Object.keys(error).length !== 0) {
            this.setState({ error: error });
            this.setState({ msg: update(this.state.msg, { type: { $set: 'error' }, header: { $set: 'Error!!' }, content: { $set: 'please fill the form correctly.' } }) })
        }
        else {
            this.setState({ msg: update(this.state.msg, { type: { $set: 'success' }, header: { $set: 'Success!!' }, content: { $set: 'Form submitted successfully.' } }) })
        }
    }

    render() {
        const {userDetail} = this.state
        return (
            <div>
                 
                < UserDetail
                   isFetching={this.state.loader}
                    error={this.state.error}
                    detail={userDetail}
                    Change={this.handleACchange}
                    geoChange={this.handlegeoChange} 
                    submit={this.handleSubmit}
                    message={this.state.msg}
                />
            </div>

        );
    }
}
export default showUserDetail