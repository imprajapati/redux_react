import { STOP_LOADING, START_LOADING, CHANGE_NAME } from '../actions/actionLoader';
import { Header } from 'semantic-ui-react';

const initialState = {
	loader: false,
	firstName: ""
};
export default function loaderReducer(state = initialState, action) {
	switch (action.type) {
		case START_LOADING:
			return {
				...state,
				loader: true,
			};
		case STOP_LOADING:
			return {
				...state,
				loader: false,
			};
		case CHANGE_NAME:
			return {
				...state,
				firstName: action.name,
			};

		default:
			return state;
	}
}