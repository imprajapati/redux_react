import {createStore, combineReducers} from 'redux';
//import movielistReducer from './reducers/movielistReducer';
import loaderReducer from './reducers/reducerLoader';
/* const reducer = combineReducers({movies: movielistReducer});
const initialState = {
movies: {name: "TERMINATOR 2"}
}; */
const store = createStore(loaderReducer);
export default store;