//these are actions
export const STOP_LOADING = 'STOP_LOADING';
export const START_LOADING = 'START_LOADING';
export const CHANGE_NAME = 'CHANGE_NAME';

//these are action creaters
export function stopLoading() {
  return {
    type: STOP_LOADING,
    payload:false,
  };
}
export function startLoading() {
  return {
    type: START_LOADING,
    payload:true,
  };
}
  export const changeName = () => {
    return {
      type: CHANGE_NAME,
      name:'Prajapati',
    };
}

