import React, { Component } from 'react';
// import logo from './logo.svg'; 
import './App.css';
import { BrowserRouter as Router, Switch, Route, BrowserRouter} from "react-router-dom";
import Registration from './pages/registration'
import ShowBmi from './pages/showBmiDetails'
import ShowFeedback from './pages/showFeedback';
import List from './comp/routerList'
import UserList from './pages/userList';
import ShowUserDetail from './pages/showUserDetail'
import ShowAddUser from './pages/showAddUser';
import UserFunctions from './comp/userFunctions';
import ShowDetailPage from './pages/showDetailPage';
import ShowGetAllUsers from './pages/showGetAllUsers';
import showTestForm from './pages/showTestForm';

class App extends Component {

  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={List} />
            <Route exact path="/pages/userDetail/:id" component={ShowUserDetail} />
            <Route exact path="/pages/userList" component={UserList} />
            <Route exact path="/pages/registration" component={Registration} />
            <Route exact path="/pages/showBmi" component={ShowBmi} />
            <Route exact path="/pages/showFeedback" component={ShowFeedback} />
            <Route exact path="/pages/newProject" component={UserFunctions} />
            <Route exact path="/pages/showAddUser" component={ShowAddUser} />
            <Route exact path='/pages/getAllUsers' component={ShowGetAllUsers} />
            <Route exact path='/pages/usersDetail/:userNumber' component={ShowDetailPage} />
            <Route exact path='/pages/showTestForm' component={showTestForm} />
            
          </Switch>
        </BrowserRouter>

      </div>



    );

  }

}
export default App;
